import { Injectable, UnauthorizedException, Logger } from '@nestjs/common';

import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { AuthUserDto } from './dto/auth.user.dto';
import { CreateUserDto } from '../users/dto/create.user.dto';
import { User } from '../users/user.entity';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<User> {
    try {
      const user = await this.userService.findOneByUserName(username);
      if (user && (await user.validatePassword(password))) {
        return user;
      } else {
        return null;
      }
    } catch (error) {
      console.error(error.stack);
    }
  }

  async login(
    authUserDto: AuthUserDto,
  ): Promise<{ token: any; expiresIn: string; userId: any }> {
    const { userName, password } = authUserDto;
    const user = await this.validateUser(userName, password);
    if (!userName) {
      throw new UnauthorizedException('Invalid credentials');
    }
    const userId = await user.id;
    const expiresIn = process.env.TOKEN_EXPIRATION;
    const payload = { user };
    const token = await this.jwtService.signAsync(payload);

    this.logger.debug(
      `Generated JWT Token with payload ${JSON.stringify(
        payload,
      )}, ${expiresIn}, ${userId}`,
    );
    return { token, expiresIn, userId };
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();
    user.firstName = createUserDto.firstName;
    user.lastName = createUserDto.lastName;
    user.userName = createUserDto.userName;
    user.email = createUserDto.email;
    user.salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(createUserDto.password, user.salt);

    try {
      await user.save();
    } catch (error) {
      console.error(error.stack);
    }
    return user;
  }

  private async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }
}
