import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException, Logger } from '@nestjs/common';

import { AuthService } from './auth.service';
import { AuthUserDto } from './dto/auth.user.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  private logger = new Logger('localstrat');
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    const user = await this.authService.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException('Invalid user credentials');
    }
    return user;
  }

  // async validate(userName: string, password: string): Promise<any> {
  //   this.logger.debug(userName, password);
  //   const user = await this.authService.validateUser(userName, password);
  //   if (!user) {
  //     throw new UnauthorizedException('Invalid user credentials');
  //   }
  //   return user;
  // }
}
