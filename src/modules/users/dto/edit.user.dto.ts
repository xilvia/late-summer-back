export class EditUserDto {
  readonly id: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly userName: string;
  readonly email: string;
  readonly password: string;
}
