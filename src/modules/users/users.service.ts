import { Injectable, Inject } from '@nestjs/common';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import { EditUserDto } from './dto/edit.user.dto';
import { USER_REPOSITORY } from '../../core/constants';

@Injectable()
export class UsersService {
  constructor(
    @Inject(USER_REPOSITORY) private readonly userRepository: typeof User,
  ) {}

  async findAll(): Promise<User[]> {
    return await this.userRepository.findAll();
  }

  async findOneByUserName(userName: string): Promise<User> {
    return await this.userRepository.findOne({ where: { userName } });
  }

  async findOneById(id: number): Promise<User> {
    return await this.userRepository.findOne({ where: { id } });
  }

  async editUser(id: number, editUserDto: EditUserDto): Promise<User> {
    const user = await this.userRepository.findOne({ where: { id } });
    console.log(id);
    ((await user).id = id),
      ((await user).firstName = editUserDto.firstName),
      ((await user).lastName = editUserDto.lastName),
      ((await user).userName = editUserDto.userName),
      ((await user).email = editUserDto.email),
      ((await user).password = editUserDto.password),
      (await user).save({
        fields: ['firstName', 'lastName', 'userName', 'email'],
      });
    console.log((await user).id, user);
    return user;
  }

  async removeUser(id: number): Promise<void> {
    console.log(`id: ${id} deleted`);
    await this.userRepository.destroy({ where: { id } });
  }
}
