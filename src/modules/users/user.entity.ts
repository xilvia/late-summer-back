import { Table, Column, Model, DataType } from 'sequelize-typescript';
import * as bcrypt from 'bcryptjs';
import { MaxLength, IsNotEmpty, MinLength, IsEmail } from 'class-validator';

@Table
export class User extends Model<User> {
  @IsNotEmpty()
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  })
  id: number;

  @IsNotEmpty()
  @MinLength(2)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  firstName: string;

  @IsNotEmpty()
  @MinLength(2)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  lastName: string;

  @IsNotEmpty()
  @MinLength(4)
  @MaxLength(12)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  userName: string;

  @IsNotEmpty()
  @IsEmail()
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(12)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;

  @IsNotEmpty()
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  salt: string;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }
}
