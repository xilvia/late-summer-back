import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { EditUserDto } from './dto/edit.user.dto';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  //@UseGuards(AuthGuard('jwt'))
  @Get()
  async getUsers(): Promise<User[]> {
    return await this.userService.findAll();
  }

  //@UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getOne(@Param('id', ParseIntPipe) id: number): Promise<UserDto> {
    return await this.userService.findOneById(id);
  }
  
  //@UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() editUserDto: EditUserDto,
  ): Promise<UserDto> {
    console.log(editUserDto);
    return await this.userService.editUser(id, editUserDto);
  }

  //@UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param('id') id: number): Promise<void> {
    return await this.userService.removeUser(id);
  }
}
